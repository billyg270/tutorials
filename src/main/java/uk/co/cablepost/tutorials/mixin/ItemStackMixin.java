package uk.co.cablepost.tutorials.mixin;

import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.text.LiteralTextContent;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableTextContent;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import uk.co.cablepost.tutorials.client.TutorialsClient;

import java.util.List;
import java.util.Objects;

@Mixin(ItemStack.class)
public abstract class ItemStackMixin {
    @Shadow public abstract Item getItem();

    @Shadow public abstract boolean isEmpty();

    @Inject(at = @At("RETURN"), method = "getTooltip")
    public void getTooltip(@Nullable PlayerEntity player, TooltipContext context, CallbackInfoReturnable<List<Text>> cir) {
        if(player == null){
            return;
        }

        if(isEmpty()){
            return;
        }

        TutorialsClient.mouseOverItem = getItem();
        TutorialsClient.timeSinceMouseOverItem = 0;

        Identifier id = Registries.ITEM.getId(getItem());
        if(!TutorialsClient.mainTutorialForItems.containsKey(id)){
            return;
        }

        if(TutorialsClient.keyBinding.isUnbound()){
            cir.getReturnValue().add(Text.literal("You need to bind the key: '").append(Text.keybind("key.tutorials.open_tutorial").getString()).append(Text.literal("' to view tutorial")).formatted(Formatting.GOLD));
        }
        else{
            String keyName;

            String keyNameTranslationKey = TutorialsClient.keyBinding.getBoundKeyTranslationKey();
            keyName = Text.translatable(keyNameTranslationKey).getString();
            if(Objects.equals(keyNameTranslationKey, keyName)){
                String keyName2 = TutorialsClient.keyBinding.getBoundKeyLocalizedText().getContent().toString();
                if(!Objects.equals(keyName2, "")){
                    keyName = keyName2;
                }
            }

            if(TutorialsClient.timeSinceMouseTextInputFocus < 2){
                cir.getReturnValue().add(Text.literal("When a text input is not selected,").formatted(Formatting.GOLD));
            }

            cir.getReturnValue().add(Text.literal("Press [" + keyName + "] to view tutorial").formatted(Formatting.GOLD));
        }
    }
}
