package uk.co.cablepost.tutorials.client.screen;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerType;
import org.jetbrains.annotations.Nullable;

public class TutorialScreenHandler extends ScreenHandler {
    protected TutorialScreenHandler(@Nullable ScreenHandlerType<?> type, int syncId) {
        super(type, syncId);
    }

    @Override
    public ItemStack quickMove(PlayerEntity player, int slot) {
        return null;
    }

    TutorialScreenHandler(){
        this(null, 0);
    }

    @Override
    public boolean canUse(PlayerEntity player) {
        return false;
    }
}
