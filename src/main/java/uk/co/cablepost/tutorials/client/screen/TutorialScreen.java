package uk.co.cablepost.tutorials.client.screen;

import com.cinemamod.mcef.MCEF;
import com.cinemamod.mcef.MCEFBrowser;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gl.Framebuffer;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.render.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.tutorials.Tutorial;
import uk.co.cablepost.tutorials.client.TutorialsClient;

import java.util.*;

public class TutorialScreen extends HandledScreen<TutorialScreenHandler> {
    public static final Identifier BACKGROUND_TEXTURE = new Identifier(TutorialsClient.MOD_ID, "textures/gui/tutorials/tutorial.png");
    public static final Identifier BUTTONS_TEXTURE = new Identifier(TutorialsClient.MOD_ID, "textures/gui/tutorials/tutorial_buttons.png");

    private String[] tutorialOrder = null;
    private Map<String, Tutorial> tutorials = null;
    private Tutorial tutorial = null;
    private int timeSinceTutorialRelatedItemsRandomised = 0;
    private Identifier tutorialItem = null;

    private MCEFBrowser browser;

    private final Screen previousScreen;

    public TutorialScreen(PlayerInventory inventory, Text title, Screen previousScreen) {
        super(new TutorialScreenHandler(), inventory, title);
        if(previousScreen != null && previousScreen.getClass().getName().toLowerCase().contains(".ftb.")){
            this.previousScreen = previousScreen;
        }
        else {
            this.previousScreen = null;
        }
    }

    public TutorialScreen(PlayerEntity player, Screen previousScreen) {
        this(player.getInventory(), Text.translatable("container.tutorial"), previousScreen);

        //zoomInGui();

        backgroundWidth = 256 + 128;
        backgroundHeight = 224;
        titleX = 15;
        titleY = 5;
    }

    @Override
    public void onDisplayed(){
        super.onDisplayed();
        zoomInGui();
    }

    private void zoomInGui(){
        MinecraftClient minecraftClient = MinecraftClient.getInstance();

        int i = minecraftClient.getWindow().calculateScaleFactor(0, minecraftClient.forcesUnicodeFont());
        minecraftClient.getWindow().setScaleFactor(i);

        Framebuffer framebuffer = minecraftClient.getFramebuffer();
        framebuffer.resize(minecraftClient.getWindow().getFramebufferWidth(), minecraftClient.getWindow().getFramebufferHeight(), MinecraftClient.IS_SYSTEM_MAC);
        minecraftClient.gameRenderer.onResized(minecraftClient.getWindow().getFramebufferWidth(), minecraftClient.getWindow().getFramebufferHeight());
        minecraftClient.mouse.onResolutionChanged();
    }

    @Override
    protected void init(){
        super.init();

        if(tutorial != null){
            initBrowser();
        }
    }

    @Override
    public void close() {
        if(browser != null) {
            browser.close();
            browser = null;
        }

        if(this.previousScreen != null){
            MinecraftClient.getInstance().setScreen(this.previousScreen);
        }
        else {
            super.close();
        }
    }

    @Override
    public void removed() {
        if(browser != null) {
            browser.close();
            browser = null;
        }
        MinecraftClient.getInstance().onResolutionChanged();
        super.removed();
    }

    public void setTutorialsForItem(Identifier identifier) {
        if(TutorialsClient.mainTutorialForItems.containsKey(identifier)) {
            tutorialItem = identifier;

            tutorials = new HashMap<>();
            for (Map.Entry<Identifier, Tutorial> otherTutorial : TutorialsClient.tutorials.entrySet()) {
                if(otherTutorial.getValue().related_items.contains(tutorialItem.toString())){
                    tutorials.put(otherTutorial.getKey().toString(), otherTutorial.getValue());
                }
            }

            setTutorialOrder();
            setTutorial(tutorialOrder[0]);
        }
    }

    public void setTutorialOrder(){
        tutorialOrder = new String[tutorials.size()];
        int added = 0;
        while(added < tutorials.size()) {
            String highest = null;
            for (Map.Entry<String, Tutorial> entry : tutorials.entrySet()) {
                if(Arrays.stream(tutorialOrder).noneMatch(x -> Objects.equals(x, entry.getKey()))){//not already in tutorialOrder array
                    if(highest == null || tutorials.get(highest).priority < entry.getValue().priority){//highest of left to add
                        highest = entry.getKey();
                    }
                }
            }
            tutorialOrder[added] = highest;
            added++;
        }
    }

    public void setTutorial(String tut) {
        tutorial = null;

        if(tutorials.containsKey(tut)) {
            tutorial = tutorials.get(tut);
            initBrowser();
        }
    }

    private void initBrowser(){
        if (browser == null) {
            browser = MCEF.createBrowser(tutorial.url, false);
            browser.resize(101 * 3, 194 * 3);
        }
        else {
            browser.loadURL(tutorial.url);
        }
    }

    @Override
    protected void drawBackground(DrawContext context, float delta, int mouseX, int mouseY) {
        int withMinusBgWidth = (this.width - this.backgroundWidth) / 2;
        int heightMinusBgHeight = (this.height - this.backgroundHeight) / 2;
        context.drawTexture(BACKGROUND_TEXTURE, withMinusBgWidth, heightMinusBgHeight, 0, 0, 256, backgroundHeight);
        context.drawTexture(BACKGROUND_TEXTURE, withMinusBgWidth + 200, heightMinusBgHeight, 128, 0, 100, backgroundHeight);
        context.drawTexture(BACKGROUND_TEXTURE, withMinusBgWidth + 256, heightMinusBgHeight, 128, 0, 128, backgroundHeight);

        //context.drawTexture(BUTTONS_TEXTURE, withMinusBgWidth + 256, heightMinusBgHeight, 128, 0, 128, backgroundHeight);

        if(tutorials != null) {
            int c = 0;
            //for (Map.Entry<String, Tutorial> entry : tutorials.entrySet()) {
            for (String s : tutorialOrder) {
                //Tutorial tut = entry.getValue();
                Tutorial tut = tutorials.get(s);
                boolean mouseOver = mouseX >= withMinusBgWidth + 130 + 2 && mouseX <= withMinusBgWidth + 130 + 2 + 235 && mouseY >= heightMinusBgHeight + 15 + c && mouseY <= heightMinusBgHeight + 15 + c + 20;
                if (Objects.equals(tut.display_name, tutorial.display_name)) {
                    if (mouseOver) {
                        context.drawTexture(BUTTONS_TEXTURE, withMinusBgWidth + 130 + 2, heightMinusBgHeight + c + 15, 0, 120, 235, 20);
                    } else {
                        context.drawTexture(BUTTONS_TEXTURE, withMinusBgWidth + 130 + 2, heightMinusBgHeight + c + 15, 0, 0, 235, 20);
                    }
                } else {
                    if (mouseOver) {
                        context.drawTexture(BUTTONS_TEXTURE, withMinusBgWidth + 130 + 2, heightMinusBgHeight + c + 15, 0, 40, 235, 20);
                    } else {
                        context.drawTexture(BUTTONS_TEXTURE, withMinusBgWidth + 130 + 2, heightMinusBgHeight + c + 15, 0, 20, 235, 20);
                    }
                }
                c += 20;
            }
        }
    }

    @Override
    protected void drawMouseoverTooltip(DrawContext context, int x, int y) {
        int withMinusBgWidth = (this.width - this.backgroundWidth) / 2;
        int heightMinusBgHeight = (this.height - this.backgroundHeight) / 2;

        int related_c = 0;
        for (String related_item : tutorial.related_items) {
            Identifier id = new Identifier(related_item);
            if(related_c < 14 && !Objects.equals(tutorialItem.toString(), id.toString())) {
                ItemStack related_item_item_stack = new ItemStack(Registries.ITEM.get(id));
                int rx = withMinusBgWidth + 130 + 2 + (related_c * 18);
                int ry = heightMinusBgHeight + 190;
                if (x >= rx && x <= rx + 16 && y >= ry && y <= ry + 16) {
                    context.drawTooltip(this.textRenderer, related_item_item_stack.getName(), x, y);
                    timeSinceTutorialRelatedItemsRandomised = 0;
                }
                related_c++;
            }
        }
    }

    @Override
    protected void drawForeground(DrawContext context, int mouseX, int mouseY){
        if(tutorial != null){
            context.drawText(this.textRenderer, Text.translatable("container.tutorial.for").append(Text.literal(" ")).append(Registries.ITEM.get(tutorialItem).getName()), this.titleX, this.titleY, 0x404040, false);
            if(tutorial.error_message != null && !tutorial.error_message.isEmpty()){
                String[] errorParts = tutorial.error_message.split("(?<=\\G.....................................)");
                for(int i = 0; i < errorParts.length; i++) {
                    context.drawText(this.textRenderer, errorParts[i], 30, 30 + (i * 15), 0xFF4040, false);
                }
            }
        }
        else{
            context.drawText(this.textRenderer, this.title, this.titleX, this.titleY, 0x404040, false);
        }

        if(tutorial.related_items.toArray().length > 1){
            context.drawText(this.textRenderer, "Related items:", 130 + 2, 180, 0x404040, false);
        }

        int related_c = 0;
        for (String related_item : tutorial.related_items) {
            Identifier id = new Identifier(related_item);
            if(related_c < 13 && !Objects.equals(tutorialItem.toString(), id.toString())) {
                ItemStack related_item_item_stack = new ItemStack(Registries.ITEM.get(id));
                assert this.client != null;
                context.drawItem(related_item_item_stack, 130 + 2 + (related_c * 18), 190, 0);
                related_c++;
            }
        }

        if(tutorials != null) {
            int c = 0;
            //for (Map.Entry<String, Tutorial> entry : tutorials.entrySet()) {
            for (String s : tutorialOrder) {
                //Tutorial tut = entry.getValue();
                Tutorial tut = tutorials.get(s);
                context.drawText(this.textRenderer, tut.display_name, 130 + 2 + 5, c + 15 + 6, 0xc6c6c6, false);
                c += 20;
            }
        }
    }

    @Override
    public void handledScreenTick() {
        super.handledScreenTick();

        if(browser != null){
            browser.executeJavaScript(
                "if(document.getElementsByClassName(\"video-stream html5-main-video\").length > 0){" +
                        "if(document.getElementsByClassName(\"ytp-shorts-mode\").length == 0){" +
                            "document.getElementsByClassName(\"video-stream html5-main-video\")[0].style.top = \"20vh\";" +
                            "document.getElementsByClassName(\"video-stream html5-main-video\")[0].style.height = \"60vh\";" +
                        "}" +
                        "document.getElementsByClassName(\"ytp-fullscreen-button ytp-button\")[0].style.visibility = \"hidden\";" +
                        "document.getElementsByClassName(\"ytp-chrome-top-buttons\")[0].style.visibility = \"hidden\";" +
                    "}",
                "",
                0
            );
        }
    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        if(client == null){
            return super.mouseClicked(mouseX, mouseY, button);
        }

        int withMinusBgWidth = (this.width - this.backgroundWidth) / 2;
        int heightMinusBgHeight = (this.height - this.backgroundHeight) / 2;

        if(tutorials != null) {
            int c = 0;
            for (String s : tutorialOrder) {
                if (mouseX >= withMinusBgWidth + 130 + 2 && mouseX <= withMinusBgWidth + 130 + 2 + 235 && mouseY >= heightMinusBgHeight + 15 + c && mouseY <= heightMinusBgHeight + 15 + c + 20) {
                    try {
                        assert client.player != null;
                        client.player.playSound(SoundEvents.UI_BUTTON_CLICK.value(), 0.3f, 1.0f);
                        setTutorial(s);
                    } catch (Exception e) {
                        if (tutorial == null) {
                            tutorial = new Tutorial();
                            tutorial.display_name = "Tutorial that failed to load :(";
                        }
                        tutorial.error_message = "Error loading tutorial: " + e;
                    }
                }
                c += 20;
            }

            int related_c = 0;
            for (String related_item : tutorial.related_items) {
                Identifier id = new Identifier(related_item);
                if(related_c < 13 && !Objects.equals(tutorialItem.toString(), id.toString())) {
                    int rx = withMinusBgWidth + 130 + 2 + (related_c * 18);
                    int ry = heightMinusBgHeight + 190;
                    if (mouseX >= rx && mouseX <= rx + 16 && mouseY >= ry && mouseY <= ry + 16) {
                        assert client.player != null;
                        client.player.playSound(SoundEvents.UI_BUTTON_CLICK.value(), 0.3f, 1.0f);
                        setTutorialsForItem(id);
                    }
                    related_c++;
                }
            }

            if(browser != null) {
                int bmx = browserMouseX(mouseX);
                int bmy = browserMouseY(mouseY);

                if (bmx >= 0 && bmy >= 0) {
                    browser.sendMousePress(browserMouseX(mouseX), browserMouseY(mouseY), button);
                    browser.setFocus(true);
                }
            }
        }

        return super.mouseClicked(mouseX, mouseY, button);
    }

    @Override
    public boolean mouseReleased(double mouseX, double mouseY, int button) {
        if(browser != null) {
            browser.sendMouseRelease(browserMouseX(mouseX), browserMouseY(mouseY), button);
            browser.setFocus(true);
        }
        return super.mouseReleased(mouseX, mouseY, button);
    }

    @Override
    public void mouseMoved(double mouseX, double mouseY) {
        if(browser != null) {
            browser.sendMouseMove(browserMouseX(mouseX), browserMouseY(mouseY));
        }
        super.mouseMoved(mouseX, mouseY);
    }

    @Override
    public boolean mouseScrolled(double mouseX, double mouseY, double delta) {
        if(browser != null) {
            browser.sendMouseWheel(browserMouseX(mouseX), browserMouseY(mouseY), delta, 0);
        }
        return super.mouseScrolled(mouseX, mouseY, delta);
    }

    @Override
    public void render(DrawContext context, int mouseX, int mouseY, float delta) {
        super.render(context, mouseX, mouseY, delta);

        if(tutorial == null){
            timeSinceTutorialRelatedItemsRandomised = 0;
            return;
        }

        if(tutorial.related_items.toArray().length > 13) {
            timeSinceTutorialRelatedItemsRandomised++;
            if (timeSinceTutorialRelatedItemsRandomised > 200) {
                timeSinceTutorialRelatedItemsRandomised = 0;
                Collections.shuffle(tutorial.related_items);
            }
        }

        if(browser != null) {
            int topLeftX = x + 15;
            int topLeftY = y + 15;
            int width = 101;
            int height = 194;

            RenderSystem.disableDepthTest();
            RenderSystem.setShader(GameRenderer::getPositionTexColorProgram);
            RenderSystem.setShaderTexture(0, browser.getRenderer().getTextureID());
            Tessellator t = Tessellator.getInstance();
            BufferBuilder buffer = t.getBuffer();
            buffer.begin(VertexFormat.DrawMode.QUADS, VertexFormats.POSITION_TEXTURE_COLOR);
            buffer.vertex(topLeftX, topLeftY, 0).texture(0.0f, 0.0f).color(255, 255, 255, 255).next();// Top left
            buffer.vertex(topLeftX, topLeftY + height, 0).texture(0.0f, 1.0f).color(255, 255, 255, 255).next();// Bottom left
            buffer.vertex(topLeftX + width, topLeftY + height, 0).texture(1.0f, 1.0f).color(255, 255, 255, 255).next();// Bottom right
            buffer.vertex(topLeftX + width, topLeftY, 0).texture(1.0f, 0.0f).color(255, 255, 255, 255).next();// Top right
            t.draw();
            RenderSystem.setShaderTexture(0, 0);
            RenderSystem.enableDepthTest();
        }

        drawMouseoverTooltip(context, mouseX, mouseY);
    }

    private int browserMouseX(double mouseX) {
        return ((int)mouseX - (x + 15)) * 3;
    }

    private int browserMouseY(double mouseY) {
        return ((int)mouseY - (y + 15)) * 3;
    }

    @Override
    public boolean shouldPause() {
        return true;
    }
}
