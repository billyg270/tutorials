package uk.co.cablepost.tutorials;

import java.util.*;

public class Tutorial {
    public String display_name = "Unnamed Tutorial";
    public String error_message = "";
    public String url = "";

    public int priority = 0;

    public List<String> related_items = new ArrayList<>();
}
